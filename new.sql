-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2015 at 10:54 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `new`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday_tbl`
--

CREATE TABLE IF NOT EXISTS `birthday_tbl` (
`id` int(11) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday_tbl`
--

INSERT INTO `birthday_tbl` (`id`, `birthdate`) VALUES
(1, '2015-12-02'),
(2, '2010-02-03'),
(3, '2015-12-29'),
(4, '2015-12-11'),
(5, '1991-02-02');

-- --------------------------------------------------------

--
-- Table structure for table `emails_tbl`
--

CREATE TABLE IF NOT EXISTS `emails_tbl` (
`id` int(11) NOT NULL,
  `emails` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emails_tbl`
--

INSERT INTO `emails_tbl` (`id`, `emails`) VALUES
(1, 'mazhar@yahoo.com'),
(2, 'title@gmail.com'),
(3, 'Mozammel_Haque@ymail.com'),
(4, 'asdsad@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `new_tbl`
--

CREATE TABLE IF NOT EXISTS `new_tbl` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_tbl`
--

INSERT INTO `new_tbl` (`id`, `title`) VALUES
(1, 'Java'),
(2, 'Javascprit'),
(3, 'Javascprit');

-- --------------------------------------------------------

--
-- Table structure for table `summary_tbl`
--

CREATE TABLE IF NOT EXISTS `summary_tbl` (
`id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_tbl`
--

INSERT INTO `summary_tbl` (`id`, `company_name`, `summary`) VALUES
(1, 'square', 'this is good'),
(2, 'Rahad', 'he is good boy'),
(3, 'Mazhar', 'it''s a medicine company'),
(4, 'Mozammel Haque', 'good'),
(5, 'Siam', 'Need to be more time sensative.'),
(6, 'Meril', 'we are launghing new product very soon.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday_tbl`
--
ALTER TABLE `birthday_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails_tbl`
--
ALTER TABLE `emails_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_tbl`
--
ALTER TABLE `new_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_tbl`
--
ALTER TABLE `summary_tbl`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday_tbl`
--
ALTER TABLE `birthday_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `emails_tbl`
--
ALTER TABLE `emails_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `new_tbl`
--
ALTER TABLE `new_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `summary_tbl`
--
ALTER TABLE `summary_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
