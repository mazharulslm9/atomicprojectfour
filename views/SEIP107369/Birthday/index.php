<?php
ini_set('display_errors', 'Off');
include_once("../../../vendor/autoload.php");

use \App\Bitm\SEIP107369\Birthday\BirthDate;

$birthday = new BirthDate();
$bday = $birthday->index();
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Birthday</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./../../../asset/css/custom.css" rel="stylesheet">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <h3 class="text-center">Birthday List</h3>
                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <button  class="btn btn-info btn-md"> <a href="create.php">Add Birthday</a></button>
                    </div>
                    <div class="col-md-3">
                        <select style="width: 50%">
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                            <option>40</option>
                            <option>50</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button  class="btn btn-danger btn-md">Download as PDF/XL</button>

                    </div>

                    <div class=" col-md-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->

                <hr>

                <table class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th class="text-center">Sl.</th>
                            <th class="text-center">ID</th>
                            <th class="text-center">Birthdate &dArr;</th>

                            <th class="text-center" colspan="5">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $slno = 1;
                        foreach ($bday as $bd) {
                            ?>
                            <tr>
                                <td><?php echo $slno; ?></td>
                                <td><?php echo $bd->id; ?></td>
                                <td><a href="#"><?php echo date('d-m-Y', strtotime($bd->birthdate)) ?></a></td>
                                <td><button class="btn btn-success btn-md"><a href="view.php">View</a></button></td>
                                <td><button class="btn btn-warning btn-md"><a href="edit.php">Edit</a></button></td>
                                <td><button class="btn btn-danger btn-md"><a href="delete.php">Delete</a></button></td>
                                <td><button class="btn btn-danger btn-md"><a href="#">Trash/Recover </a></button></td>
                                <td><button class="btn btn-danger btn-md"><a href="#"> Email to Friend </a></button></td>


                            </tr>
                            <?php
                            $slno++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row text-center"> 

                <button class="btn btn-danger btn-md"><a href="./../../../index.php"> Back To Project List </a></button>
                <nav>
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>


    </body>
</html>


