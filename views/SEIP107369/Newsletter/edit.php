<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Subscription</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body class="bg-primary">
        <section>
            <div class="container">
                <div class="row">
                    <h3 class="text-center">Update subscriber</h3>
                    <hr>
                    <form class="form-inline text-center">
                        <div class="form-group">
                            <label for="exampleInputName2">Enter Email: </label>
                            <input type="email" class="form-control" id="exampleInputName2" placeholder="Enter Email">
                        </div>

                        <button type="submit" class="btn btn-warning btn-md">Update</button>
                    </form>
                </div>
            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <?php

        function __autoload($className) {

            $filename = '../../../' . str_replace("\\", "/", $className) . ".php";

            include_once ($filename);
        }

        use src\Bitm\SEIP107369\Newsletter\Subscription;

$new_subscriber = new Subscription();
        $new_subscriber->edit();
        ?>
    </body>
</html>