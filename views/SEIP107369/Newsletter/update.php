<?php

function __autoload($className) {

    $filename = '../../../' . str_replace("\\", "/", $className) . ".php";

    include_once ($filename);
}

use src\Bitm\SEIP107369\Newsletter\Subscription;

$new_subscriber = new Subscription();
$new_subscriber->update();
