<?php
ini_set('display_errors','Off');
include_once './../../../vendor/autoload.php';

use App\Bitm\SEIP107369\Newsletter\Subscription;
use App\Bitm\SEIP107369\Utility\Utility;

$new_subscriber = new Subscription($_POST);
$new_subscriber->store();
