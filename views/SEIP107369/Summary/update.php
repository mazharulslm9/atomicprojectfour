<?php

function __autoload($className) {

    $filename = '../../../' . str_replace("\\", "/", $className) . ".php";

    include_once ($filename);
}

use src\Bitm\SEIP107369\Summary\SummaryofOrganization;

$new_summary = new SummaryofOrganization();
$new_summary->update();
