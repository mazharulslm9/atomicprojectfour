<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Site</title>

        <!-- Bootstrap -->
        <link href="./asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./asset/css/custom.css" rel="stylesheet">
    </head>
    <body >
        <section>
            <div class="container">

                <header> 

                    <h2 class="text-center">BITM-Web App Dev-PHP </h2>
                    <h3 class="text-center">Md.Mazharul Islam </h3>
                    <h4 class="text-center">SEIP107369</h4>
                    <h5 class="text-center">Batch-11</h5>

                </header>
                <hr>
                <h4 style="padding: 10px;" class="bg-info text-center">Atomic Projects</h4>
                <table  class="table table-bordered table-responsive">
                    <thead class="text-center">


                        <tr >
                            <th class="text-center">SL. No.</th>
                            <th class="text-center">Project Name</th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td>
                                1.
                            </td>
                            <td>
                                <a href="views/SEIP107369/Book/index.php">Book Title</a>                               
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>
                                2.
                            </td>
                            <td>
                                <a href="views/SEIP107369/Birthday/index.php">Birthday</a>                               
                            </td>
                        </tr>

                        <tr class="text-center">
                            <td>
                                3.
                            </td>
                            <td>
                                <a href="views/SEIP107369/Newsletter/index.php">Email Subscription</a>                               
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>
                                4.
                            </td>
                            <td>
                                <a href="views/SEIP107369/Summary/index.php">Company Description</a>                               
                            </td>
                        </tr>



                    </tbody>
                </table>
            </div>

        </section>

    </body>
</html>



