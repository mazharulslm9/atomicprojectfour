<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Bitm\SEIP107369\Utility;

class Utility{
    
    
    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function redirect1($url="/atomicproject-Mazharul-107369/Views/SEIP107369/Book/index.php"){
        header("Location:".$url);
    }
    static public function redirect2($url="/atomicproject-Mazharul-107369/Views/SEIP107369/Birthday/index.php"){
        header("Location:".$url);
    }
     static public function redirect3($url="/atomicproject-Mazharul-107369/Views/SEIP107369/City/index.php"){
        header("Location:".$url);
    }
    static public function redirect4($url="/AtomicProject_Mazharul_107369_B11/views/SEIP107369/Summary/index.php"){
        header("Location:".$url);
    }
}